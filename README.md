#Unity Cloud Builds Auto Fetch Server

##The following bash commands was tested on CentOS 6.8 to provide auto run on start up and auto rerun on crash:

###Show whether nginx start on boot/reboot:

	chkconfig --list nginx

###Stop nginx from starting on boot/reboot:

	chkconfig nginx off


###Globally install nodejs and npm:

	curl --silent --location https://rpm.nodesource.com/setup_6.x | bash -
	yum -y install nodejs

###Globally install forever and forever-service:

	npm install -g forever
	npm install -g forever-service

###Command to install service named "gamebuildserver" to auto start on boot/reboot

	forever-service install gamebuildserver --nologrotate --start --script /home/gamebuild/buildautofetch/server.js -o " --workingDir /home/gamebuild/buildautofetch/ -a -l /home/gamebuild/buildautofetch/log/forever.log -o /home/gamebuild/buildautofetch/log/out.log -e /home/gamebuild/buildautofetch/log/err.log" 

###Command to remove installed service named "gamebuildserver"

	forever-service delete gamebuildserver

###Manual forever command without start on boot/reboot
	
	forever start -a -l /home/gamebuild/buildautofetch/log/forever.log -o /home/gamebuild/buildautofetch/log/out.log -e /home/gamebuild/buildautofetch/log/err.log --workingDir /home/gamebuild/buildautofetch/ /home/gamebuild/buildautofetch/server.js