//Script to run using the `forever` tool:
//
//	sudo source ~/.bash_profile | nvm use 6.9.1 | forever start -a -l forever.log -o out.log -e err.log server.js

var fs = require("fs");
var path = require("path");
var express = require('express');
var ecstatic = require("ecstatic");
var bodyParser = require('body-parser');
var request = require('superagent');
var download = require('download');
var exec = require('child_process').exec;

var app = express();

var PORT = 7778;
var LOW_PRIV_GRP = "gamebuild";
var LOW_PRIV_USER = "gamebuild";

var API_KEY = "ee9984cfc339a4a501a0cdfa9ff2f427";
var AUTH_HDR = 'Basic ' + API_KEY;

var GAME_DIR = path.join(__dirname, "games");
var WHOOK_PATH = '/buildsucceeded';

app.get('/', function(req, res) {
	res.redirect("/games");
})

app.get('/test/:therest', function(req, res) {
	res.send('Hello World!' + JSON.stringify(req.params));
})

app.all('/games*', ecstatic({
	baseDir: "/games/",
	root: GAME_DIR
}));

app.use(WHOOK_PATH, bodyParser.json());
app.post(WHOOK_PATH, function(req, res) {
	console.log(req.body);
	console.log("============================================================");
	console.log("============================================================");
	console.log("============================================================");

	var info = req.body;

	if (info.buildStatus != "success") {
		res.status(200).end();
		return;
	}

	//fetch the executable:

	var gameName = info.projectName;
	var targetName = info.buildTargetName; //Difficulty (Hard - Medium - Easy)
	var buildStatusURL = "https://build-api.cloud.unity3d.com" + info.links.api_self.href;
	var zipStoreDir = path.join(GAME_DIR, gameName)

	fs.mkdir(zipStoreDir, function(e) {
		if (e && e.code != "EEXIST") {
			console.log('mkdir "' + zipStoreDir + '" has error:' + e);
		}

		request
			.get(buildStatusURL)
			.set('Authorization', AUTH_HDR)
			.set('Accept', 'application/json')
			.end(function(err, res) {
				if (err && err.status === 404) {
					console.log('Requesting build status got 404 Not Found: ' + res.body.message);
					return
				} else if (err) {
					// all other error types we handle generically
					console.log("Requesting build status got other errors: " + err);
					return
				}

				// err == nil:

				var downloadURL = res.body.links.download_primary.href;
				var fileType = res.body.links.download_primary.meta.type;
				if (fileType != "ZIP") {
					console.log("Unknown built file type: " + fileType);
				}

				download(downloadURL, zipStoreDir, {
					extract: true
				}).then(() => {
					exec('touch -m ' + zipStoreDir, {
						timeout: 5000 //milliseconds
					}, (error, stdout, stderr) => {
						if (error) {
							console.error(`touch -m error: ${error}`);
							return;
						}
					});
					console.log('Download done: ' + gameName + " ||| " + targetName);
				}).catch(function(reason) {
					console.log('Download error: ', reason);
				});
			});
	});


});

app.listen(PORT, function() {
	console.log('Listening on port ' + PORT);
	try {
		console.log('Old User ID: ' + process.getuid() + ', Old Group ID: ' + process.getgid());
		process.setgid(LOW_PRIV_GRP);
		process.setuid(LOW_PRIV_USER);
		console.log('New User ID: ' + process.getuid() + ', New Group ID: ' + process.getgid());

		try {
			fs.mkdirSync(GAME_DIR);
		} catch (e) {
			if (e.code != 'EEXIST') console.log(e);
		}
	} catch (err) {
		console.log('Cowardly refusing to keep the process alive as root.');
		process.exit(1);
	}
})